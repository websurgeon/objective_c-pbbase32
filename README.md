# PBBase32

Objective-C implementation of the Base32 encoding and decoding process described in [RFC 4648](https://bitbucket.org/websurgeon/objective_c-pbbase32/src/master/resources/Base32-RFC4648.txt).  

Details of the encrypyion/decryption process implemented are described [here](https://bitbucket.org/websurgeon/objective_c-pbbase32/src/master/resources/Base32-Process-Notes.md)  

----  
** IMPORTANT NOTE **  
This code is similar to and influenced by [ekscryptos/base32](https://github.com/ekscrypto/Base32) implementation.  Please see a note about this [here](https://bitbucket.org/websurgeon/objective_c-pbbase32/src/master/important-note.md).
----  

### Objective-C Categories

#### NSString+Base32

Methods for encoding NSData object into base32 NSString

    + (NSString *)base32StringUsingData:(NSData *)data error:(NSError **)error;

    + (NSString *)base32StringUsingData:(NSData *)data;

#### NSData+Base32

Methods for decoding a base32 encoded NSString into NSData (NSASCIIStringEncoding)

    + (NSData *)dataUsingBase32String:(NSString *)base32String error:(NSError **)error;

    + (NSData *)dataUsingBase32String:(NSString *)base32String;


### The MIT License

> Copyright (C) 2013 Peter Barclay.
>
> Permission is hereby granted, free of charge, to any person
> obtaining a copy of this software and associated documentation files
> (the "Software"), to deal in the Software without restriction,
> including without limitation the rights to use, copy, modify, merge,
> publish, distribute, sublicense, and/or sell copies of the Software,
> and to permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
>
> The above copyright notice and this permission notice shall be
> included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
> NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
> BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
> ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
> CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
