 ## IMPORTANT NOTE

    I want to make it clear that, while this is my own work, it is a similar 
    implementation to ekscrypto/Base32.
    
    My objective for this project was to fully understand 
    the Base32 encoding/decoding process, rather than 
    develope a unique solution.  

    Once I had understood the process by implementing the RFC 
    on paper I did research a number of existing Base32 implementations.

    I did not copy-paste code from any source, but one of the main implementations I was influenced by was ekscrypto/Base32.

    It is not my intention to deceive and so I hope this notice makes that clear.

Please see ekscrypto/Base32 here: [https://github.com/ekscrypto/Base32](https://github.com/ekscrypto/Base32) 