//
//  NSStreingBase32Tests.m
//  PBBase32
//
//  Created by Peter Barclay on 10/08/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//
//  MIT Licence
//

#import "NSStringBase32Tests.h"
#import "NSString+Base32.h"

@implementation NSStringBase32Tests

- (void)testEncoding1Byte
{
    [self performEncodingTestForString:@"1" expecting:@"GE======" failMessage:nil];
}

- (void)testEncoding2Bytes
{
    [self performEncodingTestForString:@"12" expecting:@"GEZA====" failMessage:nil];
}

- (void)testEncoding3Bytes
{
    [self performEncodingTestForString:@"123" expecting:@"GEZDG===" failMessage:nil];
}

- (void)testEncoding4Bytes
{
    [self performEncodingTestForString:@"1234" expecting:@"GEZDGNA=" failMessage:nil];
}

- (void)testEncoding5Bytes
{
    [self performEncodingTestForString:@"12345" expecting:@"GEZDGNBV" failMessage:nil];
}

- (void)testEncoding6Bytes
{
    [self performEncodingTestForString:@"123456" expecting:@"GEZDGNBVGY======" failMessage:nil];
}
- (void)testEncoding10Bytes
{
    [self performEncodingTestForString:@"1234567890" expecting:@"GEZDGNBVGY3TQOJQ" failMessage:nil];
}

- (void)testEncoding15Bytes
{
    [self performEncodingTestForString:@"123451234512345" expecting:@"GEZDGNBVGEZDGNBVGEZDGNBV" failMessage:nil];
}

- (void)testEncodingRFC4648TestVectors
{
    [self performEncodingTestForString:@"foo" expecting:@"MZXW6===" failMessage:nil];
    [self performEncodingTestForString:@"foob" expecting:@"MZXW6YQ=" failMessage:nil];
    [self performEncodingTestForString:@"fooba" expecting:@"MZXW6YTB" failMessage:nil];
    [self performEncodingTestForString:@"foobar" expecting:@"MZXW6YTBOI======" failMessage:nil];
}

- (void)testEncodingPhrase
{
    [self performEncodingTestForString:@"THIS IS THE HOUSE THAT JACK BUILT" expecting:@"KREESUZAJFJSAVCIIUQEQT2VKNCSAVCIIFKCASSBINFSAQSVJFGFI===" failMessage:nil];
}

- (void)testEncodingLongerPhrase
{
    [self performEncodingTestForString:@"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum" expecting:@"JRXXEZLNEBUXA43VNUQGI33MN5ZCA43JOQQGC3LFOQWCAY3PNZZWKY3UMV2HK4RAMFSGS4DJONUWG2LOM4QGK3DJOQWCA43FMQQGI3ZAMVUXK43NN5SCA5DFNVYG64RANFXGG2LENFSHK3TUEB2XIIDMMFRG64TFEBSXIIDEN5WG64TFEBWWCZ3OMEQGC3DJOF2WCLRAKV2CAZLONFWSAYLEEBWWS3TJNUQHMZLONFQW2LBAOF2WS4ZANZXXG5DSOVSCAZLYMVZGG2LUMF2GS33OEB2WY3DBNVRW6IDMMFRG64TJOMQG42LTNEQHK5BAMFWGS4LVNFYCAZLYEBSWCIDDN5WW233EN4QGG33OONSXC5LBOQXCARDVNFZSAYLVORSSA2LSOVZGKIDEN5WG64RANFXCA4TFOBZGK2DFNZSGK4TJOQQGS3RAOZXWY5LQORQXIZJAOZSWY2LUEBSXG43FEBRWS3DMOVWSAZDPNRXXEZJAMV2SAZTVM5UWC5BANZ2WY3DBEBYGC4TJMF2HK4ROEBCXQY3FOB2GK5LSEBZWS3TUEBXWGY3BMVRWC5BAMN2XA2LEMF2GC5BANZXW4IDQOJXWSZDFNZ2CYIDTOVXHIIDJNYQGG5LMOBQSA4LVNEQG6ZTGNFRWSYJAMRSXGZLSOVXHIIDNN5WGY2LUEBQW42LNEBUWIIDFON2CA3DBMJXXE5LN" failMessage:nil];
}

- (void)testEncodingNil
{
    STAssertNoThrow([NSString base32StringUsingData:nil], nil);
    STAssertNil([NSString base32StringUsingData:nil], nil);
    NSError *error = nil;
    [NSString base32StringUsingData:nil error:&error];
    STAssertNotNil(error, nil);
}

- (void)testBase32Encode
{
    NSString *testInput = @"adcd";
    NSString *expected = @"MFSGGZA=";
    STAssertEqualObjects([testInput base32Encode], expected, nil);
}

- (void)testBase32Decode
{
    NSString *testInput = @"MFSGGZA=";
    NSString *expected = @"adcd";
    STAssertEqualObjects([testInput base32Decode], expected, nil);
}

- (void)performEncodingTestForString:(NSString *)inputString expecting:(NSString *)expectedString failMessage:(NSString *)failMsg
{
    NSData *data = [inputString dataUsingEncoding:NSASCIIStringEncoding];
    NSString *base32String = [NSString base32StringUsingData:data];
    STAssertEqualObjects(base32String, expectedString, failMsg);
}

@end
