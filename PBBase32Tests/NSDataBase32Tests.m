//
//  NSDataBase32Tests.m
//  PBBase32
//
//  Created by Peter on 10/08/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "NSDataBase32Tests.h"
#import "NSData+Base32.h"

@implementation NSDataBase32Tests

- (void)testDecoding8BytesNoPadding
{
    [self performDecodingOfBase32String:@"GEZDGNBV" expectingDecodedString:@"12345" failMessage:nil];
}

- (void)testDecoding16BytesNoPadding
{
    [self performDecodingOfBase32String:@"GEZDGNBVGEZDGNBV" expectingDecodedString:@"1234512345" failMessage:nil];
}

- (void)testDecoding8BytesWith6Padding
{
    [self performDecodingOfBase32String:@"GE======" expectingDecodedString:@"1" failMessage:nil];
}

- (void)testDecoding8BytesWith4Padding
{
    [self performDecodingOfBase32String:@"GEZA====" expectingDecodedString:@"12" failMessage:nil];
}

- (void)testDecoding8BytesWith3Padding
{
    [self performDecodingOfBase32String:@"GEZDG===" expectingDecodedString:@"123" failMessage:nil];
}

- (void)testDecoding8BytesWith1Padding
{
    [self performDecodingOfBase32String:@"GEZDGNA=" expectingDecodedString:@"1234" failMessage:nil];
}

- (void)testDecoding16BytesWith6Padding
{
    [self performDecodingOfBase32String:@"GEZDGNBVGY======" expectingDecodedString:@"123456" failMessage:nil];
}

- (void)testDecodingRFC4648TestVectors
{
    [self performDecodingOfBase32String:@"MZXW6===" expectingDecodedString:@"foo" failMessage:nil];
    [self performDecodingOfBase32String:@"MZXW6YQ=" expectingDecodedString:@"foob" failMessage:nil];
    [self performDecodingOfBase32String:@"MZXW6YTB" expectingDecodedString:@"fooba" failMessage:nil];
    [self performDecodingOfBase32String:@"MZXW6YTBOI======" expectingDecodedString:@"foobar" failMessage:nil];
}

- (void)testDecodingPhrase
{
    [self performDecodingOfBase32String:@"KREESUZAJFJSAVCIIUQEQT2VKNCSAVCIIFKCASSBINFSAQSVJFGFI===" expectingDecodedString:@"THIS IS THE HOUSE THAT JACK BUILT" failMessage:nil];
}

- (void)testDecodingLongerPhrase
{
    [self performDecodingOfBase32String:@"JRXXEZLNEBUXA43VNUQGI33MN5ZCA43JOQQGC3LFOQWCAY3PNZZWKY3UMV2HK4RAMFSGS4DJONUWG2LOM4QGK3DJOQWCA43FMQQGI3ZAMVUXK43NN5SCA5DFNVYG64RANFXGG2LENFSHK3TUEB2XIIDMMFRG64TFEBSXIIDEN5WG64TFEBWWCZ3OMEQGC3DJOF2WCLRAKV2CAZLONFWSAYLEEBWWS3TJNUQHMZLONFQW2LBAOF2WS4ZANZXXG5DSOVSCAZLYMVZGG2LUMF2GS33OEB2WY3DBNVRW6IDMMFRG64TJOMQG42LTNEQHK5BAMFWGS4LVNFYCAZLYEBSWCIDDN5WW233EN4QGG33OONSXC5LBOQXCARDVNFZSAYLVORSSA2LSOVZGKIDEN5WG64RANFXCA4TFOBZGK2DFNZSGK4TJOQQGS3RAOZXWY5LQORQXIZJAOZSWY2LUEBSXG43FEBRWS3DMOVWSAZDPNRXXEZJAMV2SAZTVM5UWC5BANZ2WY3DBEBYGC4TJMF2HK4ROEBCXQY3FOB2GK5LSEBZWS3TUEBXWGY3BMVRWC5BAMN2XA2LEMF2GC5BANZXW4IDQOJXWSZDFNZ2CYIDTOVXHIIDJNYQGG5LMOBQSA4LVNEQG6ZTGNFRWSYJAMRSXGZLSOVXHIIDNN5WGY2LUEBQW42LNEBUWIIDFON2CA3DBMJXXE5LN" expectingDecodedString:@"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum" failMessage:nil];
}

- (void)testDencodingNil
{
    STAssertNoThrow([NSData dataUsingBase32String:nil], nil);
    STAssertNil([NSData dataUsingBase32String:nil], nil);
    NSError *error = nil;
    [NSData dataUsingBase32String:nil error:&error];
    STAssertNotNil(error, nil);
}

- (void)performDecodingOfBase32String:(NSString *)base32String expectingDecodedString:(NSString *)expected failMessage:(NSString *)failMessage
{
    NSData *decodedData = [NSData dataUsingBase32String:base32String];
    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSASCIIStringEncoding];
    STAssertEqualObjects(decodedString, expected, failMessage);
}

- (void)testInvalidInputLengthDoesIsNotDecoded
{
    NSData *decodedData = [NSData dataUsingBase32String:@"GAYDAMBQa"];
    STAssertNil(decodedData, nil);
}

- (void)testInvalidInputLengthCausesError
{
    NSError *error = nil;
    NSData *decodedData = [NSData dataUsingBase32String:@"GAYDAMBQa" error:&error];
    STAssertNil(decodedData, nil);
    STAssertNotNil(error, nil);
}

@end
