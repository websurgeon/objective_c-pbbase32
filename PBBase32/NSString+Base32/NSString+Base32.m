//
//  NSString+Base32.m
//  PBBase32
//
//  Created by Peter Barclay on 09/08/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//
//  MIT Licence
//
// See Base32-Process-Notes.md for more detail on how this encoding algorithm works
// NB: This code is similar to and infuenced by ekscrypto's base32 implementation https://github.com/ekscrypto/Base32


#import "NSString+Base32.h"
#import "NSData+Base32.h"

#define NSSTRING_BASE32_ERROR_DOMAIN @"com.pbbase32.encoding.error"

#define EXCEPTION_No_Data @"NoDataException"
#define EXCEPTION_MESSAGE_No_Data @"No data supplied"

#define EXCEPTION_Memory_Allocation @"FailedMemoryAllocationException"
#define EXCEPTION_MESSAGE_Memory_Allocation @"Failed to allocate memory for encoded result"


@implementation NSString (Base32)

+ (NSString *)base32StringUsingData:(NSData *)data error:(NSError **)error;
{
    NSString *base32String = nil;
    unsigned char *encoded = NULL;

    @try {
        
        
        static char base32Alphabet[32] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
        static char paddingChar = '=';
        
        // convert NSData to bytes
        unsigned char *input = (unsigned char *)[data bytes];
        encoded = NULL;
        
        // calculate block information
        NSUInteger bytesInInput = [data length];
        
        if (bytesInInput == 0) {
            @throw [NSException exceptionWithName:EXCEPTION_No_Data reason:NSLocalizedString(EXCEPTION_MESSAGE_No_Data, @"exception message when no data supplied for encoding") userInfo:nil];
        }
        // allocate memory for encoded result
        NSUInteger fullBlockCount = bytesInInput / 5; // 1 block is 5 bytes
        NSUInteger bytesInPartialBlock = bytesInInput % 5;
        NSUInteger encodedLength = (fullBlockCount + (bytesInPartialBlock > 0 ? 1 : 0)) * 8;
        encoded = malloc(encodedLength);
        
        if (encoded == NULL) {
            @throw [NSException exceptionWithName:EXCEPTION_Memory_Allocation reason:NSLocalizedString(EXCEPTION_MESSAGE_Memory_Allocation, @"exception message when memory allocation fails") userInfo:nil];
        }

        
        // initialise counters
        NSUInteger blockBaseIndex = 0; // keep track of byte that starts each block of encoded 8-bytes
        NSUInteger blockIndex = 0; // keep track of which block we are dealing with
        NSUInteger byteIndex = 0; // keep track of byte that starts each block of 5-bytes
        
        // only last block can be a partial block so better performance can be achieved by
        // handling last partial block separately (if required)
        unsigned char b1, b2, b3, b4, b5;
        
        // loop through each full block of 5-bytes
        while (blockIndex < fullBlockCount) {
            
            b1 = input[byteIndex    ];
            b2 = input[byteIndex + 1];
            b3 = input[byteIndex + 2];
            b4 = input[byteIndex + 3];
            b5 = input[byteIndex + 4];
            
            // convert block 5-bytes into encoded 8-bytes
            encoded[blockBaseIndex    ] = base32Alphabet[((b1 >> 3) & 0x1f)                     ];
            encoded[blockBaseIndex + 1] = base32Alphabet[((b1 << 2) & 0x1c) | ((b2 >> 6) & 0x03)];
            encoded[blockBaseIndex + 2] = base32Alphabet[((b2 >> 1) & 0x1f)                     ];
            encoded[blockBaseIndex + 3] = base32Alphabet[((b2 << 4) & 0x10) | ((b3 >> 4) & 0x0f)];
            encoded[blockBaseIndex + 4] = base32Alphabet[((b3 << 1) & 0x1e) | ((b4 >> 7) & 0x01)];
            encoded[blockBaseIndex + 5] = base32Alphabet[((b4 >> 2) & 0x1f)                     ];
            encoded[blockBaseIndex + 6] = base32Alphabet[((b4 << 3) & 0x18) | ((b5 >> 5) & 0x07)];
            encoded[blockBaseIndex + 7] = base32Alphabet[((b5     ) & 0x1f)                     ];
            
            // increment counters ready for next block
            blockBaseIndex += 8;
            blockIndex += 1;
            byteIndex += 5;
        }
        
        // now we need to check if there is a partial block that still needs encoding
        
        if (bytesInPartialBlock > 0) {
            b1 = 0;
            b2 = 0;
            b3 = 0;
            b4 = 0;
            b5 = 0;
            switch (bytesInPartialBlock) {
                case 4:
                    b4 = input[byteIndex + 3];
                case 3:
                    b3 = input[byteIndex + 2];
                case 2:
                    b2 = input[byteIndex + 1];
                case 1:
                    b1 = input[byteIndex];
                    
                    encoded[blockBaseIndex    ] = base32Alphabet[((b1 >> 3) & 0x1f)                     ];
                    encoded[blockBaseIndex + 1] = base32Alphabet[((b1 << 2) & 0x1c) | ((b2 >> 6) & 0x03)];
                    encoded[blockBaseIndex + 2] = base32Alphabet[((b2 >> 1) & 0x1f)                     ];
                    encoded[blockBaseIndex + 3] = base32Alphabet[((b2 << 4) & 0x10) | ((b3 >> 4) & 0x0f)];
                    encoded[blockBaseIndex + 4] = base32Alphabet[((b3 << 1) & 0x1e) | ((b4 >> 7) & 0x01)];
                    encoded[blockBaseIndex + 5] = base32Alphabet[((b4 >> 2) & 0x1f)                     ];
                    encoded[blockBaseIndex + 6] = base32Alphabet[((b4 << 3) & 0x18) | ((b5 >> 5) & 0x07)];
                    encoded[blockBaseIndex + 7] = base32Alphabet[((b5     ) & 0x1f)                     ];
                    
                    break;
            }
            
            // append padding char to encoded output
            static char padMap[5] = {0, 6, 4, 3, 1};
            NSUInteger padding = padMap[bytesInPartialBlock];
            
            blockBaseIndex = encodedLength - padding;
            while(padding-- > 0) {
                encoded[blockBaseIndex++] = paddingChar;
            }
        }
        
        //convert encoded bytes to output string
        base32String = [[NSString alloc] initWithBytes:encoded length:encodedLength encoding:NSASCIIStringEncoding];
        
    }
    @catch (NSException *exception) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:NSSTRING_BASE32_ERROR_DOMAIN code:1 userInfo:@{@"message":[exception reason], @"exceptoin":exception}];
        }
    }
    @finally {
        if(encoded != NULL ) {
            // release memory used for encoding
            free(encoded);
        }
    }
    
    return base32String;
}

+ (NSString *)base32StringUsingData:(NSData *)data
{
    return [NSString base32StringUsingData:data error:NULL];
}

- (NSString *)base32Encode
{
    return [NSString base32StringUsingData:[self dataUsingEncoding:NSASCIIStringEncoding] error:nil];
}

- (NSString *)base32Decode
{
    return [[NSString alloc] initWithData:[NSData dataUsingBase32String:self] encoding:NSASCIIStringEncoding];
}

@end
