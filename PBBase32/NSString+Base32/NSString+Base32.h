//
//  NSString+Base32.h
//  PBBase32
//
//  Created by Peter Barclay on 09/08/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//
//  MIT Licence
//

#import <Foundation/Foundation.h>
#import "NSData+Base32.h"

@interface NSString (Base32)

+ (NSString *)base32StringUsingData:(NSData *)data error:(NSError **)error;

+ (NSString *)base32StringUsingData:(NSData *)data;

- (NSString *)base32Encode;

- (NSString *)base32Decode;

@end
